# Makefile for the sshexport project

VERS=$(shell sed <sshexport -n -e '/version *= *\(.*\)/s//\1/p')

MANDIR=/usr/share/man/man1
BINDIR=/usr/bin

DOCS    = README COPYING sshexport.xml
SOURCES = sshexport Makefile $(DOCS)

.PHONY: pylint dist clean release

all: sshexport-$(VERS).tar.gz

install: sshexport.1
	cp sshexport $(BINDIR)
	gzip <sshexport.1 >$(MANDIR)/sshexport.1.gz

sshexport.1: sshexport.xml
	xmlto man sshexport.xml

sshexport.html: sshexport.xml
	xmlto html-nochunks sshexport.xml

sshexport-$(VERS).tar.gz: $(SOURCES) sshexport.1
	mkdir sshexport-$(VERS)
	cp $(SOURCES) sshexport-$(VERS)
	tar -czf sshexport-$(VERS).tar.gz sshexport-$(VERS)
	rm -fr sshexport-$(VERS)
	ls -l sshexport-$(VERS).tar.gz

PYLINTOPTS = --rcfile=/dev/null --reports=n \
	--msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" \
	--dummy-variables-rgx='^_'
SUPPRESSIONS = --disable=C0103,C0301,C0302,C0330,C1001,W0105,W0110,W0141,W0142,W0231,W0401,W0404,W0612,W0611,W0614,W0621,W0702,R0201,R0902,R0903,R0904,R0911,R0912,R0913,R0914,R0915,C0111
PY3K_SUPPRESSIONS = --disable="C0325,F0401,E1101"
pylint:
	@pylint $(PYLINTOPTS) $(SUPPRESSIONS) $(PY3K_SUPPRESSIONS) sshexport


dist: sshexport-$(VERS).tar.gz

clean:
	rm -f sshexport.1 sshexport.html
	rm -f *.1 MANIFEST index.html

release: sshexport-$(VERS).tar.gz sshexport.html
	shipper version=$(VERS) | sh -e -x

refresh: sshexport.html
	shipper -N -w version=$(VERS) | sh -e -x
